import del from "del"
import nodeResolve from "rollup-plugin-node-resolve"
import typescript from "rollup-plugin-typescript2"
import { dependencies } from "./package.json"

export default del(["dist", "lib"]).then(() => ({
  input: "src/index.ts",
  plugins: [
    nodeResolve({ main: false, jsnext: true, modulesOnly: true }),
    typescript({ useTsconfigDeclarationDir: true })
  ],
  external: Object.keys(dependencies),
  output: [
    { format: "cjs", file: "dist/index.cjs.js" },
    { format: "esm", file: "dist/index.esm.js" }
  ]
}))
