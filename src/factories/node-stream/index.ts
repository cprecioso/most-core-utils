import { Stream } from "@most/types"
import { FromStream } from "./FromStream"

declare namespace fromNodeStream {
  interface Options {
    endEventName?: string
    dataEventName?: string
    errorEventName?: string
  }
}

const fromNodeStream = <T extends string | Buffer | any>(
  nodeStream: NodeJS.ReadableStream,
  options: fromNodeStream.Options = {}
): Stream<T> => {
  const {
    endEventName = "end",
    dataEventName = "data",
    errorEventName = "error"
  } = options

  if (typeof nodeStream.pause === "function") {
    nodeStream.pause()
  }

  return new FromStream<T>(
    nodeStream,
    endEventName,
    dataEventName,
    errorEventName
  )
}

export { fromNodeStream }
