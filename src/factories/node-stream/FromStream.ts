import { MulticastSource, never } from "@most/core"
import { Disposable, Scheduler, Sink } from "@most/types"
import { addListeners } from "./addListeners"
import { FromStreamDisposable } from "./FromStreamDisposable"

export class FromStream<
  T extends string | Buffer | any
> extends MulticastSource<T> {
  constructor(
    private readonly stream: NodeJS.ReadableStream,
    private readonly endEventName: string,
    private readonly dataEventName: string,
    private readonly errorEventName: string
  ) {
    super(never())
  }

  private _disposable?: Disposable

  run(sink: Sink<T>, scheduler: Scheduler): Disposable {
    const n = this.add(sink)

    if (n === 1) {
      this._disposable = addListeners(
        this.stream,
        this.endEventName,
        this.dataEventName,
        this.errorEventName,
        sink,
        scheduler
      )

      if (typeof this.stream.resume === "function") this.stream.resume()
    }

    return new FromStreamDisposable(this, sink)
  }
}
