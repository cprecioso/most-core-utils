import {
  propagateEndTask,
  propagateErrorTask,
  propagateEventTask
} from "@most/core"
import { asap } from "@most/scheduler"
import { Disposable, Scheduler, Sink } from "@most/types"

export function addListeners(
  stream: NodeJS.ReadableStream,
  endEventName: string,
  dataEventName: string,
  errorEventName: string,
  sink: Sink<any>,
  scheduler: Scheduler
): Disposable {
  const evt = (value: any) => asap(propagateEventTask(value, sink), scheduler)
  const err = (err: Error) => asap(propagateErrorTask(err, sink), scheduler)
  const end = () => asap(propagateEndTask(sink), scheduler)

  stream.addListener(dataEventName, evt)
  stream.addListener(endEventName, end)
  stream.addListener(errorEventName, err)

  return {
    dispose() {
      stream.removeListener(dataEventName, evt)
      stream.removeListener(endEventName, end)
      stream.removeListener(errorEventName, err)
    }
  }
}
