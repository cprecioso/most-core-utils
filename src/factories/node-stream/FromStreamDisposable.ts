import { Disposable, Sink } from "@most/types"
import { FromStream } from "./FromStream"

export class FromStreamDisposable implements Disposable {
  private disposed = false

  constructor(
    private readonly source: FromStream<any>,
    private readonly sink: Sink<any>
  ) {}

  dispose() {
    if (this.disposed) return
    this.disposed = true
    const remaining = this.source.remove(this.sink)
    return remaining === 0 && (this.source as any)._dispose()
  }
}
