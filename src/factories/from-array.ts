import { propagateTask, PropagateTask } from "@most/core"
import { asap } from "@most/scheduler"
import { Scheduler, Sink, Stream } from "@most/types"

export function fromArray<T>(arr: Array<T>): Stream<T> {
  return new ArraySource(arr)
}

class ArraySource<T> implements Stream<T> {
  constructor(private readonly _arr: Array<T>) {}

  run(sink: Sink<T>, scheduler: Scheduler) {
    return asap(
      propagateTask(
        function(this: PropagateTask<T[], T>, t, array, sink) {
          for (var i = 0, l = array.length; i < l && this.active; ++i) {
            sink.event(t, array[i])
          }
          this.active && sink.end(t)
        },
        this._arr,
        sink
      ),
      scheduler
    )
  }
}
