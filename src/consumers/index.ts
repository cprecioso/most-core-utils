import { runEffects, tap } from "@most/core"
import { Scheduler, Stream } from "@most/types"

export const forEach = <A>(
  fn: (val: A) => void,
  stream: Stream<A>,
  scheduler: Scheduler
): Promise<void> => runEffects(tap(fn, stream), scheduler)

export const reduce = <A, B>(
  fn: (acc: B, val: A) => B,
  seed: B,
  stream: Stream<A>,
  scheduler: Scheduler
) => forEach(val => (seed = fn(seed, val)), stream, scheduler).then(() => seed)

export const toArray = <A>(stream: Stream<A>, scheduler: Scheduler) =>
  reduce<A, A[]>((arr, v) => (arr.push(v), arr), [], stream, scheduler)
